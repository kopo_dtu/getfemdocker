FROM debian:bullseye
RUN apt-get update && apt-get install -y \
    make \
    autoconf \
    automake \
    libblas-dev \
    libmetis-dev \
    libmumps-seq-dev \
    libqhull-dev \
    libsuperlu-dev \
    libtool \
    python3-all-dev \
    python3-numpy \
    python3-scipy \
    wget \
    ; \
    rm -rf /var/lib/apt/lists/*; \
    ln -s /usr/bin/python3 /usr/bin/python; \
    ln -s /usr/bin/pip3 /usr/bin/pip;
RUN wget http://git.savannah.nongnu.org/cgit/getfem.git/snapshot/getfem-5.4.1.tar.gz; \
    mkdir -p /usr/src/getfem; \
    tar -xzf getfem-5.4.1.tar.gz -C /usr/src/getfem --strip 1; \
    rm getfem-5.4.1.tar.gz;
RUN cd /usr/src/getfem; \
	./autogen.sh; \
	./configure --prefix=/usr \
               --enable-shared \
               --with-pic \
               --enable-metis \
               --disable-matlab \
               --disable-superlu \
               --enable-mumps \
               --disable-scilab \
               --with-mumps="-lsmumps_seq -ldmumps_seq -lcmumps_seq -lzmumps_seq -lpord_seq" \
   ; \
   make -j "$(nproc)"; \
   make install;
